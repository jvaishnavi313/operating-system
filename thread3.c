#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

void* fun(){
	
	printf("Start fun\n");
	printf(" Fun : %ld",pthread_self());
	
	for(int i=0;i<=2;i++){
		
		printf("FUN i = %d\n",i++);
	}

	pthread_exit(NULL);
	printf("\nENd fun\n");

}

void* gun(){
	
	printf("Start Gun\n");
	printf(" Gun : %ld",pthread_self());
	
	pthread_t tid;
	pthread_create(&tid,NULL,fun,NULL);
	
	for(int i=0;i<=5;i++){
        	sleep(2);
                printf("GUN i = %d\n",i++);
        }

	//pthread_join(tid,NULL);
	pthread_exit(NULL);
	printf("\nENd Gun\n");

}

void main(){
	
	pthread_t tid;
	printf("Start main\n");
	printf("MAin %ld\n",pthread_self());
	
	
	pthread_create(&tid,NULL,gun,NULL);

	pthread_join(tid,NULL);


	printf("End main\n");
}

