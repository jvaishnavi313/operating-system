#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>

int main(){
	
	int pid=fork();
	
	if(pid>1){
		//parent
		wait(NULL);
		for(int i=1;i<=5;i++){
			printf("Parent pid: %d\n",getpid());
			printf("Parent ppid: %d\n",getppid());
		}
	}else if(pid==0){
		//child
		for(int i=1;i<=10;i++){
			printf("child pid: %d\n",getpid());
			printf("child ppid: %d\n",getppid());
			sleep(2);
		}
	}else{
		return -1;
	}
}
