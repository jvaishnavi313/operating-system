#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>

int main(){
	
	int pid=fork();
	
	if(pid>1){
		//parent
		//wait(NULL);
		printf("Parent pid: %d\n",getpid());
		printf("Parent ppid: %d\n",getppid());
		
	}else if(pid==0){
		//child
		
		printf("child pid: %d\n",getpid());
		printf("child ppid: %d\n",getppid());
		
		execlp("/bin/ps","shashi",NULL);
	}else{
		return -1;
	}
}
