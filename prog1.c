#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>

int main(){
	
	int pid=fork();
	
	if(pid>1){
		//parent
		//wait(NULL);
		for(int i=1;i<=5;i++){
			printf("Parent pid: %d\n",getpid());
			printf("Parent ppid: %d\n",getppid());
		}
	}else if(pid==0){
		
		execlp("./prog2","abc",NULL);
	}else{
		return -1;
	}
}
