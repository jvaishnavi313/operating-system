#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>

int main(){
	
	int pid=fork();
	
	if(pid>1){
		//parent
		printf("Parent pid: %d\n",getpid());
		printf("Parent ppid: %d\n",getppid());
		sleep(10);
	}else if(pid==0){
		//child
		
		printf("child pid: %d\n",getpid());
		printf("child ppid: %d\n",getppid());
	}else{
		return -1;
	}
}
